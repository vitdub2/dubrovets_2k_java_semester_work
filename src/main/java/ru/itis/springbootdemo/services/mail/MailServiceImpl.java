package ru.itis.springbootdemo.services.mail;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassRelativeResourceLoader;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.SpringTemplateLoader;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;
import java.util.TreeMap;

@Component
public class MailServiceImpl implements MailService {
    private Template template;

    @Value("${spring.mail.username}")
    private String emailFrom;

    @Autowired
    private JavaMailSender mailSender;

    public MailServiceImpl() {
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_0);
        configuration.setDefaultEncoding("UTF-8");
        configuration.setTemplateLoader(
                new SpringTemplateLoader(
                        new ClassRelativeResourceLoader(this.getClass()), "/"
                )
        );
        configuration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        try {
            this.template = configuration.getTemplate("mail/confirm_mail.ftlh");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendConfirmEmail(String email, String username, String code) {
        Map<String, String> attributes = new TreeMap<>();
        attributes.put("username", username);
        attributes.put("code", code);

        StringWriter stringWriter = new StringWriter();
        try {
            this.template.process(attributes, stringWriter);
        } catch (TemplateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String mailText = stringWriter.toString();

        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom(emailFrom);
            messageHelper.setTo(email);
            messageHelper.setText(mailText, true);
            messageHelper.setSubject("Test mail");
        };

        mailSender.send(messagePreparator);
    }
}
