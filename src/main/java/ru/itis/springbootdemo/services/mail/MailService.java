package ru.itis.springbootdemo.services.mail;

public interface MailService {
    void sendConfirmEmail(String email, String username, String code);
}
