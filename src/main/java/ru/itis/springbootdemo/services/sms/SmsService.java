package ru.itis.springbootdemo.services.sms;

public interface SmsService {
    void sendMessage (String phone, String text);
}
