package ru.itis.springbootdemo.services.confirm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import ru.itis.springbootdemo.models.User;
import ru.itis.springbootdemo.models.UserState;
import ru.itis.springbootdemo.repositories.UserRepository;
import ru.itis.springbootdemo.services.mail.MailService;
import ru.itis.springbootdemo.services.sms.SmsService;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Component
public class ConfirmServiceImpl implements ConfirmService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SmsService smsService;

    @Autowired
    private MailService mailService;

    @Autowired
    private Environment environment;

    @Override
    public void confirmUserByCode (String code) {
        User user = userRepository.findAll().stream().filter(u -> u.getConfirmCode().equals(code)).findFirst().orElse(User.builder().build());
        if (user.getUsername() != null) {
            user.setState(UserState.CONFIRMED);
            userRepository.save(user);
        }
    }

    @Override
    public void sendConfirmSmsByUsernameAndPhone(String username, String phone) {
        User user = userRepository.findFirstByUsername(username);
        String host = null;
        try {
            host = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        String port = environment.getProperty("server.port");
        String address = "http://" + host + ":" + port + "/confirm/" + user.getConfirmCode();
        String text = "Ссылка для подтверждения аккаунта: " + address;
        smsService.sendMessage(phone, text);
    }

    @Override
    public void sendConfirmEmail(String username) {
        User user = userRepository.findFirstByUsername(username);
        String email = user.getEmail();
        String code = user.getConfirmCode();
        mailService.sendConfirmEmail(email, username, code);
    }
}
