package ru.itis.springbootdemo.services.confirm;

public interface ConfirmService {
    void confirmUserByCode (String code);
    void sendConfirmSmsByUsernameAndPhone (String username, String phone);
    void sendConfirmEmail (String username);
}
