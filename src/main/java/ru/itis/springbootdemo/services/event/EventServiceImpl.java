package ru.itis.springbootdemo.services.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import ru.itis.springbootdemo.dto.forms.events.EventForm;
import ru.itis.springbootdemo.models.*;
import ru.itis.springbootdemo.models.Calendar;
import ru.itis.springbootdemo.repositories.*;
import ru.itis.springbootdemo.services.calendar.CalendarService;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class EventServiceImpl implements EventService {

    @Autowired
    private CalendarRepository calendarRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private EventInfoRepository eventInfoRepository;

    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private CalendarService calendarService;

    @Override
    public Set<Event> getEventsByUser(String user) {
        Set<Calendar> calendars = calendarService.getCalendarsByUser(user);
        Set<Set<Event>> events = calendars.stream().map(Calendar::getEvents).collect(Collectors.toSet());
        Set<Event> trueEvents = new HashSet<>();
        events.forEach(trueEvents::addAll);
        return trueEvents;
    }

    @Override
    public List<Event> getEventSearch(String q, Integer page, Integer size, String sortParameter, String directionParameter) {
        Sort.Direction direction = Sort.Direction.ASC;
        if (directionParameter != null) {
            direction = Sort.Direction.fromString(directionParameter);
        }
        Sort sort = Sort.by(direction, "id");
        if (sortParameter != null) {
            sort = Sort.by(direction, sortParameter);
        }
        PageRequest request = PageRequest.of(page, size, sort);
        Page<EventInfo> infos = eventInfoRepository.search(q, request);
        return infos.getContent().stream().map(info -> eventRepository.getFirstByEventInfo(info)).collect(Collectors.toList());
    }

    @Override
    public void createEvent(EventForm form, Long calendarId, String author) {
        Calendar calendar = calendarRepository.getOne(calendarId);
        Profile creator = profileRepository.getByCalendar_IdAndRole(calendarId, ProfileRole.EDITOR).stream().findFirst().orElse(null);
        EventInfo eventInfo = eventInfoRepository.save(
                EventInfo.builder()
                        .name(form.getName())
                        .color(form.getColor())
                        .description(form.getDescription())
                        .place(form.getPlace())
                        .site(form.getSite())
                        .build()
        );
        Event event = eventRepository.save(
                Event.builder()
                        .creator(creator)
                        .eventInfo(eventInfo)
                        .build()
        );
        eventRepository.save(event);
        calendar.addEvent(event);
        calendarRepository.save(calendar);
    }

    @Override
    public void updateEvent(EventForm form, Long eventId) {
        Event event = eventRepository.getOne(eventId);
        EventInfo eventInfo = event.getEventInfo();

        eventInfo.setName(form.getName());
        eventInfo.setDescription(form.getDescription());
        eventInfo.setColor(form.getColor());
        eventInfo.setPlace(form.getPlace());
        eventInfo.setSite(form.getSite());

        eventInfoRepository.save(eventInfo);
    }

    @Override
    public void deleteEvent(Long eventId, Long calendarId) {
        Event event = eventRepository.getOne(eventId);
        Calendar calendar = calendarRepository.getOne(calendarId);
        EventInfo info = event.getEventInfo();
        calendar.setEvents(
                calendar.getEvents().stream().filter(event1 -> !event1.equals(event)).collect(Collectors.toSet())
        );
        calendarRepository.save(calendar);
        eventRepository.deleteById(eventId);
        eventInfoRepository.delete(info);
    }

    @Override
    public Event getEventById(Long eventId) {
        return eventRepository.getOne(eventId);
    }
}
