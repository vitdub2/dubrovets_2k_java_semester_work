package ru.itis.springbootdemo.services.event;

import ru.itis.springbootdemo.dto.forms.events.EventForm;
import ru.itis.springbootdemo.models.Event;

import java.util.List;
import java.util.Set;

public interface EventService {
    Set<Event> getEventsByUser (String username);
    List<Event> getEventSearch (String q, Integer page, Integer size, String sort, String direction);
    void createEvent (EventForm form, Long calendarId, String authorUsername);
    void updateEvent (EventForm form, Long eventId);
    void deleteEvent (Long eventId, Long calendarId);
    Event getEventById (Long eventId);
}
