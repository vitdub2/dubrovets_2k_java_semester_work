package ru.itis.springbootdemo.services.file;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import ru.itis.springbootdemo.models.FileInfo;
import ru.itis.springbootdemo.repositories.FileInfoRepository;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@Component
public class FileServiceImpl implements FileService {

    @Value("${storage.path}")
    private String storagePath;

    @Autowired
    private FileInfoRepository fileInfoRepository;

    @Override
    public String saveFile(MultipartFile file) {
        String storageName = UUID.randomUUID() + "." + FilenameUtils.getExtension(file.getOriginalFilename());
        FileInfo fileInfo = FileInfo.builder()
                .originFilename(file.getOriginalFilename())
                .size(file.getSize())
                .storageFilename(storageName)
                .type(file.getContentType())
                .url(storagePath + "/" + storageName)
                .build();
        fileInfoRepository.save(fileInfo);

        try {
            Files.copy(file.getInputStream(), Paths.get(storagePath, storageName));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fileInfo.getStorageFilename();
    }

    @Override
    public void writeFileToResponse(String filename, HttpServletResponse response) {
        FileInfo info = fileInfoRepository.getFirstByStorageFilename(filename);
        String url = info.getUrl();
        response.setContentType(info.getType());
        try {
            IOUtils.copy(new FileInputStream(url), response.getOutputStream());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
