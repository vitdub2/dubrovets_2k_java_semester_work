package ru.itis.springbootdemo.services.file;

import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;

public interface FileService {
    String saveFile(MultipartFile file);
    void writeFileToResponse(String filename, HttpServletResponse response);
}
