package ru.itis.springbootdemo.services.auth;

import ru.itis.springbootdemo.dto.forms.auth.SignInForm;
import ru.itis.springbootdemo.dto.forms.auth.SignUpForm;
import ru.itis.springbootdemo.models.User;

import java.util.Optional;

public interface AuthService {
    Optional<Long> singIn(SignInForm form);
    User signUp(SignUpForm form);
}
