package ru.itis.springbootdemo.services.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.itis.springbootdemo.dto.forms.auth.SignInForm;
import ru.itis.springbootdemo.dto.forms.auth.SignUpForm;
import ru.itis.springbootdemo.models.User;
import ru.itis.springbootdemo.models.UserState;
import ru.itis.springbootdemo.repositories.UserRepository;

import java.util.Optional;
import java.util.UUID;

@Component
public class AuthServiceImpl implements AuthService{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Optional<Long> singIn(SignInForm form) {
        return Optional.empty();
    }

    @Override
    public User signUp(SignUpForm form) {
        User user = User.builder()
                .email(form.getEmail())
                .password(passwordEncoder.encode(form.getPassword()))
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .username(form.getUsername())
                .state(UserState.NOT_CONFIRMED)
                .confirmCode(UUID.randomUUID().toString().substring(0, 6))
                .avatar("https://eitrawmaterials.eu/wp-content/uploads/2016/09/person-icon.png")
                .build();

        userRepository.save(user);
        return user;
    }
}
