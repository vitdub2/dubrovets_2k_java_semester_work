package ru.itis.springbootdemo.services.user;

import ru.itis.springbootdemo.dto.UserDto;

import java.util.List;

public interface UserService {
    List<UserDto> getAllUsers();
    UserDto getUserById(Long id);
    UserDto getUserByEmail(String email);
    UserDto getUserByUsername (String username);
    void setAvatarForUser (String username, String avatarUrl);
}
