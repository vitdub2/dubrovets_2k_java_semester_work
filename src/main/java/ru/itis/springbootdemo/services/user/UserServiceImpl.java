package ru.itis.springbootdemo.services.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itis.springbootdemo.dto.UserDto;
import ru.itis.springbootdemo.models.User;
import ru.itis.springbootdemo.models.UserState;
import ru.itis.springbootdemo.repositories.UserRepository;

import java.util.List;

@Component
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<UserDto> getAllUsers() {
        return UserDto.from(userRepository.findAll());
    }

    @Override
    public UserDto getUserById(Long id) {
        return UserDto.from(
                userRepository.findById(id)
                        .orElse(User.builder().build())
        );
    }

    @Override
    public UserDto getUserByEmail(String email) {
        return UserDto.from(
                userRepository.findFirstByEmail(email)
        );
    }

    @Override
    public UserDto getUserByUsername(String username) {
        return UserDto.from(userRepository.findFirstByUsername(username));
    }

    @Override
    public void setAvatarForUser(String username, String avatarUrl) {
        User user = userRepository.findFirstByUsername(username);
        user.setAvatar(avatarUrl);
        userRepository.save(user);
    }
}
