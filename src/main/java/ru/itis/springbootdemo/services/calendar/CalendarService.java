package ru.itis.springbootdemo.services.calendar;

import ru.itis.springbootdemo.dto.forms.calendar.CalendarCreateForm;
import ru.itis.springbootdemo.models.Calendar;

import java.util.Set;

public interface CalendarService {
    Set<Calendar> getCalendarsByUser (String username);
    void createCalendar (CalendarCreateForm form, String authorUsername);
    Calendar getCalendarById (Long calendarId);
}
