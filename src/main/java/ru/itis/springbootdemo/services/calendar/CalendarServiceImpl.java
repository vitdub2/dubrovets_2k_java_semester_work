package ru.itis.springbootdemo.services.calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itis.springbootdemo.dto.forms.calendar.CalendarCreateForm;
import ru.itis.springbootdemo.models.Calendar;
import ru.itis.springbootdemo.models.Profile;
import ru.itis.springbootdemo.models.ProfileRole;
import ru.itis.springbootdemo.models.User;
import ru.itis.springbootdemo.repositories.CalendarRepository;
import ru.itis.springbootdemo.repositories.ProfileRepository;
import ru.itis.springbootdemo.repositories.UserRepository;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class CalendarServiceImpl implements CalendarService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CalendarRepository calendarRepository;

    @Autowired
    private ProfileRepository profileRepository;

    @Override
    public void createCalendar(CalendarCreateForm form, String authorUsername) {
        User author = userRepository.findFirstByUsername(authorUsername);
        Calendar newCalendar = calendarRepository.save(
                Calendar.builder()
                        .name(form.getName())
                        .build()
        );

        profileRepository.save(
                Profile.builder()
                        .role(ProfileRole.EDITOR)
                        .user(author)
                        .calendar(newCalendar)
                        .build()
        );

        calendarRepository.save(newCalendar);
    }

    @Override
    public Set<Calendar> getCalendarsByUser(String username) {
        User user = userRepository.findFirstByUsername(username);
        Set<Profile> profileSet = profileRepository.getAllByUser(user);
        return profileSet.stream().map(Profile::getCalendar).collect(Collectors.toSet());
    }

    @Override
    public Calendar getCalendarById(Long calendarId) {
        return calendarRepository.getOne(calendarId);
    }
}
