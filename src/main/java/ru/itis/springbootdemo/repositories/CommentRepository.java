package ru.itis.springbootdemo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.springbootdemo.models.Comment;

public interface CommentRepository extends JpaRepository<Comment, Long> {
}
