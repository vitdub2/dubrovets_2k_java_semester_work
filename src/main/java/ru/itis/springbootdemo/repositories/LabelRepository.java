package ru.itis.springbootdemo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.springbootdemo.models.Label;

public interface LabelRepository extends JpaRepository<Label, Long> {
}
