package ru.itis.springbootdemo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.springbootdemo.models.Event;
import ru.itis.springbootdemo.models.EventInfo;

import java.util.Set;

public interface EventRepository extends JpaRepository<Event, Long> {
    Set<Event> getAllByCreator_Calendar_Id (Long calendarId);
    Set<Event> getAllByCreator_Id (Long creatorId);
    Event getFirstByEventInfo (EventInfo eventInfo);
}
