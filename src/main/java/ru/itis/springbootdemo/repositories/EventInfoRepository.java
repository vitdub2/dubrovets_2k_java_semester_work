package ru.itis.springbootdemo.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itis.springbootdemo.models.EventInfo;

public interface EventInfoRepository extends JpaRepository<EventInfo, Long> {
    @Query("select info from EventInfo info where (:q is null or upper(info.name) like upper(concat('%', :q, '%')))")
    Page<EventInfo> search(@Param("q") String q, Pageable pageable);
}
