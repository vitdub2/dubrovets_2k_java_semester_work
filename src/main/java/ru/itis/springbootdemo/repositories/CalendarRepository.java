package ru.itis.springbootdemo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.springbootdemo.models.Calendar;


public interface CalendarRepository extends JpaRepository<Calendar, Long> {
}
