package ru.itis.springbootdemo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.springbootdemo.models.Profile;
import ru.itis.springbootdemo.models.ProfileRole;
import ru.itis.springbootdemo.models.User;

import java.util.Set;

public interface ProfileRepository extends JpaRepository<Profile, Long> {
    Set<Profile> getByCalendar_IdAndRole(Long calendarId, ProfileRole role);
    Set<Profile> getAllByUser (User user);
}
