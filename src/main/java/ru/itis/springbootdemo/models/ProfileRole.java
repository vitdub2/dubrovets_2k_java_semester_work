package ru.itis.springbootdemo.models;

public enum ProfileRole {
    EDITOR,
    PARTICIPANT
}
