package ru.itis.springbootdemo.models;

public enum UserState {
    NOT_CONFIRMED,
    CONFIRMED
}
