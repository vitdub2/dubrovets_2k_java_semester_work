package ru.itis.springbootdemo.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "calendar")
@Entity
public class Calendar implements Comparable<Calendar>, Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(fetch = FetchType.LAZY)
    private Set<Event> events;

    private String name;

    @Override
    public int compareTo(Calendar o) {
        if (o == null) {
            return -1;
        }

        return this.name.compareTo(o.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    public void addEvent (Event event) {
        this.events.add(event);
    }
}
