package ru.itis.springbootdemo.dto.forms.calendar;

import lombok.Data;

@Data
public class CalendarCreateForm {
    private String name;
}
