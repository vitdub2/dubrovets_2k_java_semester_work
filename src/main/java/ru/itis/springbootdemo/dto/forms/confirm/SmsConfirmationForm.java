package ru.itis.springbootdemo.dto.forms.confirm;

import lombok.Data;

@Data
public class SmsConfirmationForm {
    private String phone;
}
