package ru.itis.springbootdemo.dto.forms.auth;

import lombok.Data;

@Data
public class SignUpForm {
    private String email;
    private String password;

    private String username;
    private String firstName;
    private String lastName;
}
