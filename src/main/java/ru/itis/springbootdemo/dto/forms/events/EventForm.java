package ru.itis.springbootdemo.dto.forms.events;

import lombok.Data;
import java.util.Date;

@Data
public class EventForm {
    private String name;
    private String description;
    private String place;
    private String color;
    private String site;
}
