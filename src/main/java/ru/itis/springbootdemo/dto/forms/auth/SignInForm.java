package ru.itis.springbootdemo.dto.forms.auth;

import lombok.Data;

@Data
public class SignInForm {
    private String email;
    private String password;
}
