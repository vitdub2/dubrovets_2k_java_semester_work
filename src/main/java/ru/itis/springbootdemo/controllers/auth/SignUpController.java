package ru.itis.springbootdemo.controllers.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import ru.itis.springbootdemo.dto.forms.auth.SignUpForm;
import ru.itis.springbootdemo.services.auth.AuthService;

@Controller
public class SignUpController {

    @Autowired
    private AuthService authService;

    @GetMapping("/signUp")
    public String getSignUpPage() {
        return "sign_up_page";
    }

    @PostMapping("/signUp")
    public String signUp(SignUpForm form) {
        authService.signUp(form);
        return "redirect:/users";
    }
}
