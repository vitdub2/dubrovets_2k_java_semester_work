package ru.itis.springbootdemo.controllers.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import ru.itis.springbootdemo.dto.forms.auth.SignInForm;
import ru.itis.springbootdemo.services.auth.AuthService;

@Controller
public class SignInController {

    @Autowired
    private AuthService authService;

    @GetMapping("/signIn")
    public String getSignInPage() {
        return "sign_in_page";
    }

    @PostMapping("/signIn")
    public String signIn(SignInForm form) {
        authService.singIn(form);
        return "users_page";
    }
}
