package ru.itis.springbootdemo.controllers.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.itis.springbootdemo.dto.forms.events.EventForm;
import ru.itis.springbootdemo.models.Calendar;
import ru.itis.springbootdemo.models.Event;
import ru.itis.springbootdemo.services.calendar.CalendarService;
import ru.itis.springbootdemo.services.event.EventService;

@Controller
public class EventController {

    @Autowired
    private EventService eventService;

    @Autowired
    private CalendarService calendarService;

    @GetMapping("/calendar/{calendar-id}/addEvent")
    private String getCreateEventPage (Model model, @PathVariable("calendar-id") Long calendarId) {
        model.addAttribute("calendarId", calendarId);
        return "create_event_page";
    }

    @PostMapping("/calendar/{calendar-id}/addEvent")
    private String createNewEvent (@PathVariable("calendar-id") Long calendarId, EventForm form, Authentication authentication) {
        eventService.createEvent(form, calendarId, ((UserDetails) authentication.getPrincipal()).getUsername());
        return "redirect:/calendar/" + calendarId;
    }

    @GetMapping("/calendar/{calendar-id}/event/{event-id}")
    private String getEventPage (@PathVariable("calendar-id") Long calendarId, @PathVariable("event-id") Long eventId, Model model) {
        Event event = eventService.getEventById(eventId);
        Calendar calendar = calendarService.getCalendarById(calendarId);
        model.addAttribute("event", event.getEventInfo());
        model.addAttribute("eventId", eventId);
        model.addAttribute("calendar", calendar);
        return "event_page";
    }

    @GetMapping("/calendar/{calendar-id}/event/{event-id}/edit")
    private String getEventEditPage (@PathVariable("calendar-id") Long calendarId, @PathVariable("event-id") Long eventId, Model model) {
        Event event = eventService.getEventById(eventId);
        model.addAttribute("event", event.getEventInfo());
        model.addAttribute("eventId", event.getId());
        model.addAttribute("calendarId", calendarId);
        return "event_edit_page";
    }

    @PostMapping("/calendar/{calendar-id}/event/{event-id}/edit")
    private String getEventEditPage (@PathVariable("calendar-id") Long calendarId, @PathVariable("event-id") Long eventId, EventForm form) {
        eventService.updateEvent(form, eventId);
        return "redirect:/calendar/" + calendarId + "/event/" + eventId;
    }

    @GetMapping("/calendar/{calendar-id}/event/{event-id}/delete")
    private String getEventEditPage (@PathVariable("calendar-id") Long calendarId, @PathVariable("event-id") Long eventId) {
        eventService.deleteEvent(eventId, calendarId);
        return "redirect:/calendar/" + calendarId;
    }
}
