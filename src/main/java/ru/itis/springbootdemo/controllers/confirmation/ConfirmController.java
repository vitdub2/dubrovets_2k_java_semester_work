package ru.itis.springbootdemo.controllers.confirmation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.itis.springbootdemo.dto.UserDto;
import ru.itis.springbootdemo.dto.forms.confirm.SmsConfirmationForm;
import ru.itis.springbootdemo.services.confirm.ConfirmService;
import ru.itis.springbootdemo.services.user.UserService;

@Controller
public class ConfirmController {

    @Autowired
    private ConfirmService confirmService;

    @Autowired
    private UserService userService;

    @GetMapping("/confirm")
    public String getConfirmPage (Authentication authentication, Model model) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        UserDto user = userService.getUserByUsername(userDetails.getUsername());
        model.addAttribute("username", user.getUsername());
        model.addAttribute("email", user.getEmail());
        return "confirm_page";
    }

    @PostMapping("/confirm/{username}/bySms")
    public String confirmBySms (@PathVariable("username") String username, SmsConfirmationForm form) {
        confirmService.sendConfirmSmsByUsernameAndPhone(username, form.getPhone());
        return "redirect:/profile";
    }

    @PostMapping("/confirm/{username}/byEmail")
    public String confirmByEmail (@PathVariable("username") String username) {
        confirmService.sendConfirmEmail(username);
        return "redirect:/profile";
    }

    @GetMapping("/confirm/{code}")
    public String confirmAccount(@PathVariable("code") String code) {
        confirmService.confirmUserByCode(code);
        return "success_confirm";
    }
}
