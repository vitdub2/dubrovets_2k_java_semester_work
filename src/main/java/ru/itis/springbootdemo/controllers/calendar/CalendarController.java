package ru.itis.springbootdemo.controllers.calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.itis.springbootdemo.dto.forms.calendar.CalendarCreateForm;
import ru.itis.springbootdemo.models.Calendar;
import ru.itis.springbootdemo.models.Event;
import ru.itis.springbootdemo.services.calendar.CalendarService;

import java.util.Set;

@Controller
public class CalendarController {

    @Autowired
    private CalendarService calendarService;

    @GetMapping("/calendars/create")
    public String getCreateCalendarPage () {
        return "create_calendar_page";
    }

    @PostMapping("/calendars/create")
    public String createNewCalendar (CalendarCreateForm form, Authentication authentication) {
        calendarService.createCalendar(form, ((UserDetails) authentication.getPrincipal()).getUsername());
        return "redirect:/profile";
    }

    @GetMapping("/calendars")
    public String getCalendars (Authentication authentication, Model model) {
        Set<Calendar> calendars = calendarService.getCalendarsByUser(((UserDetails)authentication.getPrincipal()).getUsername());
        if (calendars != null) {
            model.addAttribute("calendars", calendars);
        }
        return "calendars_page";
    }

    @GetMapping("/calendar/{calendar-id}")
    public String getCalendar (@PathVariable("calendar-id") Long calendarId, Model model) {
        Calendar calendar = calendarService.getCalendarById(calendarId);
        Set<Event> events = calendar.getEvents();
        model.addAttribute("events", events);
        model.addAttribute("calendar", calendar);
        return "calendar_page";
    }
}
