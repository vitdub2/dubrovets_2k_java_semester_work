package ru.itis.springbootdemo.controllers.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.itis.springbootdemo.models.Event;
import ru.itis.springbootdemo.repositories.EventRepository;
import ru.itis.springbootdemo.services.event.EventService;

import java.util.List;

@RestController
public class ApiEventController {

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private EventService eventService;

    @GetMapping("/api/events")
    @CrossOrigin(origins = "http://localhost:3000")
    public List<Event> getEvents () {
        return eventRepository.findAll();
    }

    @GetMapping("/api/events/search")
    @CrossOrigin(origins = "http://localhost:3000")
    public List<Event> getEventSearch(
            @RequestParam("q") String q,
            @RequestParam("page") Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "3") Integer size,
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "direction", required = false) String direction
    ) {
        return eventService.getEventSearch(q, page, size, sort, direction);
    }

    @GetMapping("/api/events/{event-id}")
    @CrossOrigin(origins = "http://localhost:3000")
    public Event getEventById (@PathVariable("event-id") Long eventId) {
        return eventRepository.findById(eventId).get();
    }
}
