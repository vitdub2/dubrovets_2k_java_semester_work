package ru.itis.springbootdemo.controllers.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.itis.springbootdemo.dto.UserDto;
import ru.itis.springbootdemo.dto.forms.auth.SignUpForm;
import ru.itis.springbootdemo.models.User;
import ru.itis.springbootdemo.repositories.UserRepository;
import ru.itis.springbootdemo.services.auth.AuthService;

import java.util.List;

@RestController
public class ApiUserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthService authService;

    @GetMapping("/api/users")
    @CrossOrigin(origins = "http://localhost:3000")
    public List<User> getUsers () {
        return userRepository.findAll();
    }

    @GetMapping("/api/users/{user-id}")
    @CrossOrigin(origins = "http://localhost:3000")
    public User getUserById (@PathVariable("user-id") Long userId) {
        return userRepository.getOne(userId);
    }

    @PostMapping("/api/users")
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity<User> createUser (@RequestBody SignUpForm form) {
        User user = authService.signUp(form);
        return ResponseEntity.ok(user);
    }

    @DeleteMapping("/api/users/{user-id}")
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity<User> deleteUser (@PathVariable("user-id") Long userId) {
        User user = userRepository.getOne(userId);
        userRepository.delete(user);
        return ResponseEntity.ok(user);
    }

    @PutMapping("/api/users/{user-id}")
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity<User> editUser (@RequestBody UserDto dto, @PathVariable("user-id") Long userId) {
        User user = userRepository.getOne(userId);
        user.setFirstName(user.getFirstName() == null ? dto.getFirstName() : user.getFirstName());
        user.setLastName(user.getLastName() == null ? dto.getLastName() : user.getLastName());
        user.setUsername(user.getUsername() == null ? dto.getUsername() : user.getUsername());
        user.setEmail(user.getEmail() == null ? dto.getEmail() : user.getEmail());

        userRepository.save(user);
        return ResponseEntity.ok(user);
    }
}
