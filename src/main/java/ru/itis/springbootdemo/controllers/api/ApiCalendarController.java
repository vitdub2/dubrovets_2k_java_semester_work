package ru.itis.springbootdemo.controllers.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.itis.springbootdemo.models.Calendar;
import ru.itis.springbootdemo.repositories.CalendarRepository;

import java.util.List;

@RestController
public class ApiCalendarController {

    @Autowired
    private CalendarRepository calendarRepository;

    @GetMapping("/api/calendars")
    @CrossOrigin(origins = "http://localhost:3000")
    public List<Calendar> getCalendars () {
        return calendarRepository.findAll();
    }

    @GetMapping("/api/calendars/{calendar-id}")
    @CrossOrigin(origins = "http://localhost:3000")
    public Calendar getCalendarById (@PathVariable("calendar-id") Long calendarId) {
        return calendarRepository.findById(calendarId).get();
    }
}
