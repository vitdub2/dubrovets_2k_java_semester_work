package ru.itis.springbootdemo.controllers.profile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.itis.springbootdemo.dto.UserDto;
import ru.itis.springbootdemo.models.UserState;
import ru.itis.springbootdemo.services.user.UserService;

@Controller
public class ProfileController {

    @Autowired
    private UserService userService;

    @GetMapping("/profile")
    public String getProfilePage(Authentication authentication, Model model) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        UserDto user = userService.getUserByUsername(userDetails.getUsername());
        model.addAttribute("avatar", user.getAvatar());
        model.addAttribute("username", user.getUsername());
        model.addAttribute("isConfirmed", user.getState().equals(UserState.CONFIRMED));
        return "profile";
    }
}
